﻿# 压制效果静态盲测工具

## 方法学
  本工具基于如下想法  
  1. 逐帧静态比较差异*1显著于动态观看，因此如果截图比较已经很难区分，则可以假定压制效果比较满意。  
  2. 同位置切换比左右对比更敏感更适合人眼。（也就是大家通常推崇avspmod工具的原因）  
  3. ABX测试*2比预先知道压缩模式/是否源盘更加科学。（不一定适合压制参数研究）  

  不赞同想法1，3者请自行使用其它工具；2可以探讨改进，欢迎提供补丁。  

  *1 谢谢 [vcb-s的压缩参数研制思路探讨](http://vcb-s.nmm-hd.org/Dark%20Shrine/%5BVCB-Studio%5D%5B%E6%95%99%E7%A8%8B11%5D%E7%BC%96%E7%A0%81%E5%99%A8%E5%8F%82%E6%95%B0%E7%A0%94%E5%8F%91%E6%96%B9%E6%B3%95/%5BVCB-Studio%5D%5B%E6%95%99%E7%A8%8B11%5D%E7%BC%96%E7%A0%81%E5%99%A8%E5%8F%82%E6%95%B0%E7%A0%94%E5%8F%91%E6%96%B9%E6%B3%95.pdf)  
  *2 很多思路来自于 [Hydrogenaudio的听音测试](http://wiki.hydrogenaud.io/index.php?title=Hydrogenaudio_Listening_Tests)

## 功能和界面
  本工具用javascript编写，当鼠标移到图上，切换不同截图；会随机打乱截图出现的顺序（先出现的可能是原始截图，也可能压制后截图）。让人作出全部判断后，等到最后才告知猜测成功率。  
  如果成功率不高，则假定压制效果已经接近源文件。为了偷懒，建议采用10个对8个水平。

## 视频截屏脚本

### 命名方式
  1. 源盘remux以origin_000001.png origin_000002.png ... 命名
  2. 压制后截图以encode_000001.png encode_000002.png ... 命名  
如果粗略比较，也可以用高品质的jpeg

### 截图脚本
  1. 可以自行截图对应帧
  2. ffmpeg_compare.sh（windows用户可以用 Windows Subsystem for Linux）
     将ffmpeg放到路径中即可，建议采用ffmpeg官方上链接的Linux Static Builds的nighty git版本。  
```xx.sh     
     ffmpeg_compare.sh origin_file encode_file [step_of_frames] [image_format]
     ffmpeg_compare.sh GOT_S06E10_Remux.mkv GOT_S06E10.BD.1080p.x265crf22.10bit.aac2_0-XFF.mkv 8179 jpeg
```
  3. 有个较早的avs脚本，暂时不支持x265，懒得改了（现在ffmpeg已经能精确截帧了）

## 使用  

用浏览器打开 avsmview.html#分辨率#文件后缀 就可以进行截图帧同屏同位置对比
例如：
```file:///tmp/abxtools/avsmview.html#1280x720#jpeg```  
任何参数都不带就是 1920x1080 分辨率，png格式 

## Demo

[jpeg 对比](http://gw6.saintlism.info/abxtools/avsmview.html#1920x1080#jpeg)  
仅供研究，版权属于对应公司
