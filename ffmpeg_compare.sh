#!/bin/bash

origin_file=$1
encode_file=$2
# bost start and step is in frames
start=5683
# 7217 is almost 5 minute
step=${3:-7217}
format=${4:-png}


for i in `seq 1 10`; do
  FRAME=$(($start+$step*$i-$step))
  SEC=$(($FRAME*1001/24000))
  XSEC=`echo "scale=3; $FRAME*1001/24000-$SEC" | bc -l | sed 's/[0-9]*\././g'`
  SS=`TZ=UTC0 printf '%(%H:%M:%S)T\n' $SEC`$XSEC
  #echo $FRAME $SEC $XSEC $SS
  ffmpeg -v 24 -ss $SS -i $origin_file -vf "showinfo[out]" -vframes 1 -q:v 1 -an -sn -y `printf 'origin_%06d.%s' $i $format` 2>&1 | grep Parsed_showinfo_0 | grep "type:"
  ffmpeg -v 24 -ss $SS -i $encode_file -pix_fmt rgb24 -vf "showinfo[out]" -vframes 1 -q:v 1 -an -sn -y `printf 'encode_%06d.%s' $i $format` 2>&1 | grep Parsed_showinfo_0 | grep "type:"
done
