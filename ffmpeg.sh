#!/bin/bash

infile=$1
outfile=$2
map=$3

# low aq-mode=2 aq-strength=0.9 psy-rd=1.5 for crf > 20 qcomp=0.6
nice -n 8 ffmpeg-10bit -i $infile $map -c:v libx265 -pix_fmt yuv420p10le -x265-params "pme=1:limit-tu=4:ctu=32:min-cu-size=8:max-tu-size=32:tu-intra-depth=2:tu-inter-depth=2:me=3:subme=4:merange=44:no-rect=1:no-amp=1:max-merge=3:temporal-mvp=1:no-early-skip=1:rskip=1:rdpenalty=0:no-tskip=1:no-tskip-fast=1:no-strong-intra-smoothing=1:no-lossless=1:no-cu-lossless=1:no-constrained-intra=1:no-fast-intra=1:no-open-gop=1:no-temporal-layers=1:interlace=0:keyint=360:min-keyint=1:scenecut=40:rc-lookahead=72:lookahead-slices=4:bframes=6:bframe-bias=0:b-adapt=2:ref=4:limit-refs=2:limit-modes=1:weightp=1:weightb=1:aq-mode=2:qg-size=16:aq-strength=0.9:cbqpoffs=-3:crqpoffs=-3:rd=4:psy-rd=1.5:rdoq-level=2:psy-rdoq=3.00:no-rd-refine=1:signhide=1:deblock=-1,-1:no-sao=1:no-sao-non-deblock=1:b-pyramid=1:cutree=1:no-intra-refresh=1:qcomp=0.60:qpmin=0:qpmax=51:qpstep=4:ipratio=1.40:pbratio=1.20:output-depth=10" -crf 22 -c:a aac -b:a 128k -ac 2 -c:s copy $outfile

#"720p=-vf scale=1280:-1 -sws_flags lanczos+full_chroma_inp"

# old aq-mode=1 aq-strength=0.8 psy-rd=2.0 for crf < 16(or 18) qcomp=0.65
# nice -n 8 ffmpeg-10bit -i $infile $map -c:v libx265 -pix_fmt yuv420p10le -x265-params "pme=1:limit-tu=4:ctu=32:min-cu-size=8:max-tu-size=32:tu-intra-depth=2:tu-inter-depth=2:me=3:subme=4:merange=44:no-rect=1:no-amp=1:max-merge=3:temporal-mvp=1:no-early-skip=1:rskip=1:rdpenalty=0:no-tskip=1:no-tskip-fast=1:no-strong-intra-smoothing=1:no-lossless=1:no-cu-lossless=1:no-constrained-intra=1:no-fast-intra=1:no-open-gop=1:no-temporal-layers=1:interlace=0:keyint=360:min-keyint=1:scenecut=40:rc-lookahead=72:lookahead-slices=4:bframes=6:bframe-bias=0:b-adapt=2:ref=4:limit-refs=2:limit-modes=1:weightp=1:weightb=1:aq-mode=1:qg-size=16:aq-strength=0.8:cbqpoffs=-3:crqpoffs=-3:rd=4:psy-rd=2.00:rdoq-level=2:psy-rdoq=3.00:no-rd-refine=1:signhide=1:deblock=-1,-1:no-sao=1:no-sao-non-deblock=1:b-pyramid=1:cutree=1:no-intra-refresh=1:qcomp=0.65:qpmin=0:qpmax=51:qpstep=4:ipratio=1.40:pbratio=1.20:output-depth=10" -crf 18 -c:a aac -b:a 128k -ac 2 -c:s copy $outfile
